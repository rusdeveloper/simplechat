using System;
using Microsoft.IdentityModel.Tokens;

namespace SimpleChat.Jwt
{
    public class JwtOptions
    {
        public string Issuer { get; set; }

        public string Audience { get; set; }

        public DateTime NotBefore => DateTime.UtcNow;

        public DateTime IssuedAt => DateTime.UtcNow;

        public string SigningKey { get; set; }

        #region Refresh Token

        public TimeSpan RefreshTokenValidFor { get; set; } = TimeSpan.FromMinutes(30);
        public string RefreshTokenSecretKey { get; set; }

        #endregion

        #region Access Token

        public TimeSpan AccessTokenValidFor { get; set; } = TimeSpan.FromMinutes(5);
        public string AccessTokenSecretKey { get; set; }

        #endregion
    }
}