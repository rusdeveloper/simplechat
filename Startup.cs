using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using SimpleChat.Data;
using SimpleChat.Handler;
using SimpleChat.Handler.Interface;
using SimpleChat.Hubs;
using SimpleChat.Jwt;

namespace SimpleChat
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {            
            services.Configure<JwtOptions>(options => 
            {
                options.Issuer = Configuration["JwtBearerOptions:Issuer"];
                options.Audience = Configuration["JwtBearerOptions:Audience"];
                options.SigningKey = Configuration["JwtBearerOptions:SigningKey"];
            });

            // Database
            services.AddDbContext<ChatContext>(options => options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));

            // JSON Web Token handler
            services.AddScoped<IJwtHandler, JwtHandler>();

            // Authentication & authorization
            string secretKey = Configuration["JwtBearerOptions:SigningKey"];
            
            SymmetricSecurityKey signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => {
                    options.RequireHttpsMetadata = false;
                    // options.ClaimsIssuer = Configuration["JwtBearerOptions:Issuer"];
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = Configuration["JwtBearerOptions:Issuer"],

                        ValidateAudience = true,
                        ValidAudience = Configuration["JwtBearerOptions:Audience"],

                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = signingKey,

                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero
                    };
                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            bool startsWithCondition = context.Request.Path.Value.StartsWith("/chat");
                            bool getTokenCondition = context.Request.Query.TryGetValue("token", out StringValues token);

                            if (startsWithCondition && getTokenCondition)
                            {
                                foreach (var tokenValue in token.ToArray())
                                {
                                    Console.WriteLine(tokenValue);
                                }
                                context.Token = token;
                            }
            
                            return Task.CompletedTask;
                        },
                        OnAuthenticationFailed = context =>
                        {
                            var te = context.Exception;
                            
                            Console.WriteLine(te.Message);

                            return Task.CompletedTask;
                        },
                        OnTokenValidated = context =>
                        {
                            return Task.CompletedTask;
                        } 
                    };
                });

            services.AddAuthorization();

            // services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // SignalR
            services.AddCors(options => 
            {
                options.AddPolicy("CorsPolicy", policy => 
                {
                    policy.AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowCredentials()
                            .AllowAnyOrigin();
                            // .WithOrigins("http://localhost:4200")
                            // .WithOrigins("http://localhost:5000");
                });
            });
            services.AddSignalR();
            
            // MVC
            services.AddMvcCore().AddJsonFormatters();

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }
            
            // app.UseHttpsRedirection();

            // SignalR
            app.UseCors("CorsPolicy");

            app.UseSignalR(routes => 
            {
                routes.MapHub<ChatHub>("/chat");
            });

            // Authentication
            app.UseAuthentication();

            // MVC
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            // Angular
            app.UseSpaStaticFiles();
            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseProxyToSpaDevelopmentServer("http://localhost:4200");
                }
            });
        }
    }
}
