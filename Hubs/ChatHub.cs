using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using SimpleChat.Data;
using SimpleChat.Models;

namespace SimpleChat.Hubs
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class ChatHub : Hub
    {
        private readonly ChatContext context;

        public ChatHub(ChatContext context)
        {
            this.context = context;
        }

        public async Task SendMessage(string messageText)
        {
            // User user = userService.Get(Context.User.Identity.Name);

            // if(user == null) await Task.FromException(new Exception("User is undefined"));

            Message message = new Message
            {
                Text = messageText,
                CreatedDate = DateTime.Now,
                ByUser = Context.User.Identity.Name
            };

            await context.Messages.AddAsync(message);
            await context.SaveChangesAsync();

            await Clients.All.SendAsync("ReceiveMessage", message);
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            return base.OnDisconnectedAsync(exception);
        }
        public override Task OnConnectedAsync()
        {
            return base.OnConnectedAsync();
        }

        // public Task SendToReceiver(string message, string targetUsername)
        // {
        //     var chatUserSender = onlineUserService.Get(Context.User.Identity.Name);
        //     var chatUserReciever = onlineUserService.Get(targetUsername);
            
        //     return Clients.Client(chatUserReciever.ConnectionId).SendAsync("SendToReceiver", message, chatUserSender);
        // }

        // public Task SendToAll(string message)
        // {
        //     var chatUserSender = onlineUserService.Get(Context.User.Identity.Name);

        //     return Clients.All.SendAsync("SendToAll", message, chatUserSender);
        // }
    }
}