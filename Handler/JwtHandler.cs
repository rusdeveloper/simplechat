using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SimpleChat.Data;
using SimpleChat.Handler.Interface;
using SimpleChat.Jwt;
using SimpleChat.Models;

namespace SimpleChat.Handler
{
    public class JwtHandler: IJwtHandler
    {
        private readonly ConcurrentDictionary<string, Token> localStorage;
        
        private readonly JwtOptions jwtOptions;

        public JwtHandler(IOptions<JwtOptions> jwtOptions)
        {
            localStorage = new ConcurrentDictionary<string, Token>();

            this.jwtOptions = jwtOptions.Value;
        }

        public Token CreateAccessToken(ClaimsIdentity identity)
        {
            var tokenValue = Generate(identity.Claims, jwtOptions.AccessTokenValidFor);

            if (tokenValue == null) return null;

            string userId = identity.Claims.First(claim => claim.Type == ClaimsIdentity.DefaultNameClaimType).Value;

            if (userId == null) return null;

            Token token = new Token
            {
                CreatedDate = DateTime.UtcNow,
                UserId = userId,
                Value = tokenValue,
                ValidFor = jwtOptions.AccessTokenValidFor
            };

            return token;
        }

        public Token CreateRefreshToken(ClaimsIdentity identity)
        {
            var tokenValue = Generate(identity.Claims, jwtOptions.RefreshTokenValidFor);

            if (tokenValue == null) return null;

            string userId = identity.Claims.First(claim => claim.Type == ClaimsIdentity.DefaultNameClaimType).Value;

            if (userId == null) return null;

            Token token = new Token
            {
                CreatedDate = DateTime.UtcNow,
                UserId = userId,
                Value = tokenValue,
                ValidFor = jwtOptions.RefreshTokenValidFor
            };

            if(!Add(token))
            {
                return null;
            }

            return token;
        }

        private string Generate(IEnumerable<Claim> claims, TimeSpan validFor)
        {
            var key = Encoding.UTF8.GetBytes(jwtOptions.SigningKey);

            var signingCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                    issuer: jwtOptions.Issuer,
                    audience: jwtOptions.Audience,
                    notBefore: jwtOptions.NotBefore,
                    claims: claims,
                    expires: DateTime.UtcNow.Add(validFor),
                    signingCredentials: signingCredentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        // Local Storage

        public int Count
        {
            get
            {
                return localStorage.Count;
            }
        }

        public Token Get(string username)
        {
            lock (localStorage)
            {
                Token token;

                if(!localStorage.TryGetValue(username, out token))
                {
                    return null;
                }

                return token;
            }
        }

        public bool Add(Token token)
        {
            lock (localStorage)
            {
                Token checkToken;

                if (localStorage.TryGetValue(token.UserId, out checkToken))
                {
                    return false;
                }

                Token addedToken = localStorage.AddOrUpdate(token.UserId, token, (key, value) => token);

                return addedToken != null;
            }
        }

        public bool Remove(string username)
        {
            // if(username == null) return false;

            lock (localStorage)
            {
                Token token;

                if (!localStorage.TryGetValue(username, out token))
                {
                    return false;
                }

                return localStorage.TryRemove(username, out token);
            }
        }

        public ClaimsIdentity CreateIdentity(string username)
        {
            var claims = new List<Claim>
			{
				new Claim(ClaimsIdentity.DefaultNameClaimType, username),
				// new Claim("jti", )
			};
 
            return new ClaimsIdentity(claims);
        }
    }
}