using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using SimpleChat.Models;

namespace SimpleChat.Handler.Interface
{
    public interface IJwtHandler
    {
        Token CreateAccessToken(ClaimsIdentity identity);
        Token CreateRefreshToken(ClaimsIdentity identity);

        Token Get(string username);
        bool Add(Token token);
        bool Remove(string username);
        ClaimsIdentity CreateIdentity(string username);
        
        // Task<bool> CancelToken(Token token);
        // Task<bool> CancelToken(string tokenValue);
        // Task<Token> GetToken(string refreshToken);
    }
}