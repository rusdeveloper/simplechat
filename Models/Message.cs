using System;
using System.ComponentModel.DataAnnotations;

namespace SimpleChat.Models
{
    public class Message
    {
        public int Id { get; set; }

        [DataType(DataType.DateTime)]
        // [DisplayFormat(DataFormatString = "{0:hh\\:mm\\:ss}", ApplyFormatInEditMode = true)]
        public DateTime CreatedDate { get; set; }

        [Required]
        public string Text { get; set; }

        public string ByUser { get; set; }
    }
}