using System;

namespace SimpleChat.Models
{
    public class User
    {
        public string ConnectionId { get; set; }
        public string Username { get; set; }

        // public DateTime LastChanged { get; set; }
    }
}