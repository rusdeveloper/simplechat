using System;
using System.ComponentModel.DataAnnotations;

namespace SimpleChat.Models
{
    public class Token
    {
        public int Id { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString="yyyy-MM-dd", ApplyFormatInEditMode = true)]
        public DateTime CreatedDate { get; set; }
        
        [Required]
        public string Value { get; set; }

        public TimeSpan ValidFor { get; set; } = TimeSpan.Zero;

        public string UserId { get; set; }

        public bool IsExpired
        {
            get
            {
                return CreatedDate.Add(ValidFor) > DateTime.Today;
            }
        }
    }
}