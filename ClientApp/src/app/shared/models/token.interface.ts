export interface Token {
    createdDate: Date;
    userId: string;
    value: string;
    isExpired: boolean;
}
