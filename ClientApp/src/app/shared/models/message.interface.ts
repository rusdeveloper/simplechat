export interface Message {
    createdDate: Date;
    text: string;
    byUser: string;
}
