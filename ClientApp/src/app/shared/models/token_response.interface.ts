import { Token } from './token.interface';

export interface TokenResponse {
    // refresh_token: Token;
    // access_token: Token;
    refreshToken: string;
    accessToken: string;
    username: string;
}
