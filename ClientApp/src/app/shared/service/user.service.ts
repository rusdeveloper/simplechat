import { Injectable } from '@angular/core';
import { User } from '../models/user.interface';

@Injectable()
export class UserService {
  private currentUser: User;

  constructor() {
    this.currentUser = { username: '' };
  }

  setUser(username: string): boolean {
    if (!username) { return false; }

    this.currentUser.username = username;

    return true;
  }

  getUser(): User {
    return this.currentUser;
  }
}
