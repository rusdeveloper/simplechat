import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BaseService } from './base.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenResponse } from '../models/token_response.interface';

@Injectable()
export class TokenService extends BaseService {
  constructor(private http: HttpClient, private jwtHelperService: JwtHelperService) {
    super();
  }

  refreshToken(accessToken, refreshToken): Observable<boolean> {
    return this.http.post<TokenResponse>(this.baseUrl + '/token/refresh', { refreshToken: refreshToken, accessToken: accessToken })
      .map(response => {
        localStorage.setItem('access_token', response.accessToken);
        localStorage.setItem('refresh_token', response.refreshToken);

        return true;
      })
      .catch(error => Observable.of(false));
  }
}
