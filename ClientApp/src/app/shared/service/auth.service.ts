import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs/Rx';

import { BaseService } from './base.service';
import { TokenResponse } from '../models/token_response.interface';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { UserService } from './user.service';

@Injectable()

export class AuthService extends BaseService {
  constructor(private http: HttpClient, private userService: UserService) {
    super();
  }

  login(username) {
    return this.http
      .post<TokenResponse>(this.baseUrl + '/auth/login', { username: username })
      .map(response => {
        localStorage.setItem('access_token', response.accessToken);
        localStorage.setItem('refresh_token', response.refreshToken);
        localStorage.setItem('currentUser', JSON.stringify({ username: response.username }));
      })
      .catch(this.handleError);
  }

  logout() {
    const user = JSON.parse(localStorage.getItem('currentUser'));
    const refreshToken = localStorage.getItem('refreshToken');

    this.http.post<string>(this.baseUrl + '/auth/logout', { refreshToken: refreshToken, username: user.username })
      .map(response => {
        localStorage.removeItem('access_token');
        localStorage.removeItem('refresh_token');
        localStorage.removeItem('currentUser');
      })
      .catch(this.handleError);
  }

  refreshToken(refreshToken: string) {
    const user = JSON.parse(localStorage.getItem('currentUser'));

    return this.http.post<TokenResponse>(this.baseUrl + '/token/refresh', { refreshToken: refreshToken, username: user.username })
      .map(response => {
        localStorage.setItem('access_token', response.accessToken);
        localStorage.setItem('refresh_token', response.refreshToken);

        return true;
      })
      .catch((error, caught) => caught);
  }
}
