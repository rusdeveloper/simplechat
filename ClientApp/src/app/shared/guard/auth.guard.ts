import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../service/auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private jwtHelperService: JwtHelperService,
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const refreshToken = localStorage.getItem('refresh_token');

    if (this.jwtHelperService.isTokenExpired(refreshToken)) {
      this.router.navigate(['login']);

      return false;
    }

    const accessToken = localStorage.getItem('access_token');

    if (this.jwtHelperService.isTokenExpired(accessToken)) {
      const isRefreshed = true;

      this.authService.refreshToken(refreshToken)
        .subscribe(result => isRefreshed);

      if (!isRefreshed) {
        this.router.navigate(['login']);

        return false;
      }
    }

    return true;
  }
}
