import { Directive, forwardRef } from '@angular/core';
import { FormControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[appUsernameValidator]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => UsernameValidatorDirective), multi: true }
  ]
})

export class UsernameValidatorDirective {
  validator: Function;

  constructor() {
    this.validator = validateUsernameFactory();
  }

  validate(formControl: FormControl) {
    return this.validator(formControl);
  }
}

function validateUsernameFactory() {
  return (formControl: FormControl) => {
    const USERNAME_REGEXP = /[А-Яа-яA-Za-z0-9_]+/;

    return USERNAME_REGEXP.test(formControl.value) ? null : { validateUsername: { valid: false } };
  };
}
