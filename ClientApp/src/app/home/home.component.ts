import { Component, OnInit, Input } from '@angular/core';
import { Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { HubConnection, HubConnectionBuilder, LogLevel } from '@aspnet/signalr';
import { User } from '../shared/models/user.interface';

// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs/Rx';

// Import rxjs
import 'rxjs/add/operator/map';
import { Message } from '../shared/models/message.interface';
import { UserService } from '../shared/service/user.service';
import { AuthService } from '../shared/service/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  private hubConnection: HubConnection;
  private datePipe: DatePipe;

  private baseUrl = 'http://localhost:5000';

  messages: string[] = [];
  messageText: string;
  currentUser: User;

  constructor(private http: HttpClient, private userService: UserService, authService: AuthService) {
    this.datePipe = new DatePipe('en-US');
  }

  ngOnInit() {
    const _self = this;

    const access_token = localStorage.getItem('access_token');

    this.hubConnection = new HubConnectionBuilder()
                              .withUrl(this.baseUrl + '/chat?token=' + access_token)
                              .configureLogging(LogLevel.Information)
                              .build();

    this.hubConnection.start()
      .then(result => this.getMessages())
      .catch(error => console.log('Connection error (' + error + ')'));

    this.hubConnection.on('ReceiveMessage', function(receivedMessage) { _self.appendMessage(_self, receivedMessage); });

    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  sendMessage() {
    this.hubConnection.send('SendMessage', this.messageText)
      .then(() => this.messageText = '')
      .catch(error => console.error(error));
  }

  getMessages() {
    this.http.get<Message>(this.baseUrl + '/api/chat/messages')
      .subscribe(message => this.acceptMessages(message));
  }

  acceptMessages(messages) {
    messages.forEach(message => this.appendMessage(this, message));
  }

  appendMessage(context, message: Message) {
    const messageValue = this.datePipe.transform(message.createdDate, 'HH:mm') +
                    ', ' + message.byUser +
                    ': ' + message.text;

    context.messages.push(messageValue);
  }
}
