import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { JwtModule, JwtHelperService, JwtModuleOptions } from '@auth0/angular-jwt';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AuthService } from './shared/service/auth.service';
import { AuthGuard } from './shared/guard/auth.guard';
import { UserModule } from './user/user.module';


export function tokenGetter() {
  return localStorage.getItem('access_token');
}

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
];

const jwtModuleOptions: JwtModuleOptions = {
  config: {
    tokenGetter: tokenGetter,
    whitelistedDomains: ['localhost:5000'],
    blacklistedRoutes: ['localhost:5000/api/auth', 'localhost:5000/api/token']
  }
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    UserModule,
    JwtModule.forRoot(jwtModuleOptions),
    RouterModule.forRoot(routes)
  ],
  providers: [JwtHelperService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }

// JWT Observable

// function tokenSetter(response: TokenResponse): boolean {
//   if (!response.accessToken || !response.refreshToken) {
//     localStorage.removeItem('access_token');
//     localStorage.removeItem('refresh_token');

//     return false;
//   }

//   localStorage.setItem('access_token', response.accessToken);
//   localStorage.setItem('refresh_token', response.refreshToken);

//   return true;
// }

// export function getJwtHttp(http: Http, options: RequestOptions) {
//   // return new JwtHttp(new JwtConfigService(jwtOptions, jwtModuleOptions), http, options);
// }
