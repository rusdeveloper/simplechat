import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AuthService } from '../shared/service/auth.service';
import { LoginComponent } from './login/login.component';
import { UsernameValidatorDirective } from '../shared/directives/username.validator.directive';
import { UserService } from '../shared/service/user.service';
import { LogoutComponent } from './logout/logout.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'login', component: LoginComponent },
      { path: 'logout', component: LogoutComponent }
    ])
  ],
  declarations: [LoginComponent, LogoutComponent, UsernameValidatorDirective],
  providers: [AuthService, UserService]
})
export class UserModule { }
