import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

import { AuthService } from '../../shared/service/auth.service';
import { User } from '../../shared/models/user.interface';

import 'rxjs/add/operator/finally';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  error: string;
  isSubmitted: boolean;
  user: User;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.isSubmitted = false;
    this.user = { username: '' };

    if (localStorage.getItem('refresh_token')) { this.authService.logout(); }
  }

  login({ value, valid }: { value: User, valid: boolean }) {
    if (!valid) { return; }

    this.isSubmitted = true;
    this.error = '';

    this.authService.login(value.username)
      .finally(() => this.isSubmitted = false)
      .subscribe(
        result => this.router.navigateByUrl('/'),
        error => this.error = error
      );
  }
}
