using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SimpleChat.Data;

namespace SimpleChat.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ChatController : Controller
    {
        private readonly ChatContext chatContext;

        public ChatController(ChatContext chatContext)
        {
            this.chatContext = chatContext;
        }

        [HttpGet("user")]
        public IActionResult GetUser() => Content($"User: {User.Identity.Name}");

        [HttpGet("messages")]
        public IActionResult GetMessages()
        {
            var messages = chatContext.Messages.Where(message => message.CreatedDate.Date.Equals(DateTime.Today));

            return Ok(messages);
        }
    }
}