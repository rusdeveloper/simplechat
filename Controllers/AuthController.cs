using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SimpleChat.Handler.Interface;
using SimpleChat.Models;
using SimpleChat.ViewModel;

namespace SimpleChat.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly IJwtHandler jwtHandler;

        public AuthController(IJwtHandler jwtHandler)
        {
            this.jwtHandler = jwtHandler;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult Login([FromBody]LoginViewModel loginViewModel)
        {
            // Invalid ModelState
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Get token
            var token = jwtHandler.Get(loginViewModel.Username);

            if (token != null && token.IsExpired)
            {
                ModelState.AddModelError("login_failure", "Пользователь с таким именем уже существует. Выберите другое имя");

                return BadRequest(ModelState);
            }
 
            // Identity
            var identity = jwtHandler.CreateIdentity(loginViewModel.Username);

            // Refresh token
            var refreshToken = jwtHandler.CreateRefreshToken(identity);

            // Access token
            var accessToken = jwtHandler.CreateAccessToken(identity);

            if(refreshToken == null || accessToken == null)
            {
                ModelState.AddModelError("login_failure", "Произошла внутренняя ошибка сервера. Повторите попытку");

                return BadRequest(ModelState);
            }

            // Http response body
            var response = new
            {
                refreshToken = refreshToken.Value,
                accessToken = accessToken.Value,
                username = loginViewModel.Username,
            };

            return Ok(response);
        }

        [Authorize]
        [HttpPost("logout")]
        public IActionResult Logout([FromBody]TokenViewModel tokenViewModel)
        {
            bool isRemoved = jwtHandler.Remove(tokenViewModel.Username);

            // Не удалось удалить пользователя
            if (!isRemoved)
            {
                ModelState.AddModelError("token_failure", "Произошла внутренняя ошибка сервера");

                return BadRequest(ModelState);
            }

            return Ok("Выход был выполнен успешно");
        }
    }
}