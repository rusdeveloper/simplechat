using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimpleChat.Handler.Interface;
using SimpleChat.Models;
using SimpleChat.ViewModel;

namespace SimpleChat.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : Controller
    {
        private readonly IJwtHandler jwtHandler;

        public TokenController(IJwtHandler jwtHandler)
        {
            this.jwtHandler = jwtHandler;
        }

        [HttpPost("refresh")]
        public IActionResult RefreshToken([FromBody] TokenViewModel tokenViewModel)
        {
            Token token = jwtHandler.Get(tokenViewModel.Username);

            // Токен не найден
            if(token == null)
            {
                ModelState.AddModelError("token_failure", "Token doesn't exists");
                
                return BadRequest(ModelState);
            }

            // Токен отозван / истекло время
            if (token.IsExpired)
            {
                ModelState.AddModelError("token_failure", "Token expired");
                
                return BadRequest(ModelState);
            }

            Token refreshToken = jwtHandler.CreateRefreshToken((ClaimsIdentity)User.Identity);
            Token accessToken = jwtHandler.CreateAccessToken((ClaimsIdentity)User.Identity);

            var response = new
            {
                refreshToken = refreshToken.Value,
                accessToken = accessToken.Value
            };

            return Ok(response);
        }

        // [HttpPost("cancel")]
        // private async Task<IActionResult> CancelToken([FromBody] TokenViewModel tokenViewModel)
        // {
        //     Token token = await jwtHandler.GetToken(tokenViewModel.RefreshToken);

        //     // Токен не найден
        //     if(token == null)
        //     {
        //         ModelState.AddModelError("token_failure", "Token doesn't exists");
                
        //         return BadRequest(ModelState);
        //     }

        //     // Отозвать токен
        //     bool isCanceled = await jwtHandler.CancelToken(token);

        //     if(!isCanceled)
        //     {
        //         ModelState.AddModelError("token_failure", "Could not cancel token");
                
        //         return BadRequest(ModelState);
        //     }

        //     return Ok("Token successfully canceled");
        // }
    }
}