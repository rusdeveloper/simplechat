using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using SimpleChat.Models;

namespace SimpleChat.Data
{
    public class ChatContext : DbContext
    {
        public ChatContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Message> Messages { get; set; }
        public DbSet<Token> Tokens { get; set; }
    }
}