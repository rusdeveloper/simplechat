using SimpleChat.Models;

namespace SimpleChat.ViewModel
{
    public class TokenViewModel
    {
        public string RefreshToken { get; set; }
        public string Username { get; set; }
    }
}