using System.ComponentModel.DataAnnotations;

namespace SimpleChat.ViewModel
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Не указано имя пользователя")]
        public string Username { get; set; }
    }
}